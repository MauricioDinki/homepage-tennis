import { tns } from 'tiny-slider/src/tiny-slider'

console.log('HEY')

let slider = tns({
  container: '.home-carrousel-wrapper',
  items: 1,
  gutter: 50,
  controls: false,
  nav: false,
  loop: false,
  responsive: {
    576: {
      gutter: 65,
      items: 3,
    },
  },
})

// eslint-disable-next-line no-undef
document.getElementById('dotOne').onclick = function () {
  slider.goTo(1)
}
// eslint-disable-next-line no-undef
document.getElementById('dotTwo').onclick = function () {
  slider.goTo(2)
}
// eslint-disable-next-line no-undef
document.getElementById('dotThree').onclick = function () {
  slider.goTo(3)
}
// eslint-disable-next-line no-undef
document.getElementById('dotFour').onclick = function () {
  slider.goTo(4)
}
// eslint-disable-next-line no-undef
document.getElementById('prev').onclick = function () {
  slider.goTo('prev')
}
// eslint-disable-next-line no-undef
document.getElementById('next').onclick = function () {
  slider.goTo('next')
}

// Import Styles
import './styl/main.styl'

import './js/carrousel'
import './js/menu'

// Import Images
require.context('./img', true, /\.(gif|png|jpe?g|svg|webp|ico)$/)
